﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics.Core;
using OpenTK.Graphics.OpenGL;
using Color = System.Drawing.Color;
using InputManager = InverseKinematics.Core.InputManager;

namespace InverseKinematics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Simulation Simulation { get; set; }


        public MainWindow()
        {
            Simulation = new Simulation();
            InitializeComponent();
            
        }

        private void DisplayControl_OnControlLoaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.VertexProgramPointSize);
            GL.LineWidth(1);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);
            Simulation.InitializeSimulation();

            CompositionTarget.Rendering += OnRender;
        }

        private void OnRender(object sender, EventArgs e)
        {
            InputManager.SetMousePosition(DisplayControl.MousePosition);
            var dt = Simulation.TimeCounter.CountDT();
            GL.ClearColor(System.Drawing.Color.White);

            Simulation.Scene.Camera.UpdateCamera(DisplayControl.MousePosition);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Simulation.Scene.SceneBackground.Render();

            Simulation.Scene.SceneCursor.Update();
            Simulation.Scene.SceneCursor.Render();

            Simulation.Scene.SceneArm.Update();
            Simulation.Scene.SceneArm.Render();

            for (var i = 0; i < Simulation.Scene.SceneObjects.Count; i++)
            {
                SceneObject item = Simulation.Scene.SceneObjects[i];
                item.Update();
                item.Render();
            }

            DisplayControl.SwapBuffers();
        }



        private void DisplayControl_OnControlResized(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.Camera.SetViewportValues(DisplayControl.AspectRatio, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
            Simulation.Storage.SetDisplayDimensions(DisplayControl.ControlWidth, DisplayControl.ControlHeight, DisplayControl.AspectRatio);
            GL.Viewport(0, 0, DisplayControl.ControlWidth, DisplayControl.ControlHeight);
        }

        private void DisplayControl_OnControlUnloaded(object sender, RoutedEventArgs e)
        {
            Simulation.Scene.SceneArm.Dispose();
            Simulation.Scene.SceneBackground.Dispose();
            Simulation.Scene.SceneCursor.Dispose();
            foreach (SceneObject item in Simulation.Scene.SceneObjects)
            {
                item.Dispose();
            }
        }

        private void DisplayControl_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Simulation.Scene.Camera.SetLastMousePosition(DisplayControl.MousePosition);
            InputManager.OnMouseButtonChange(e.ChangedButton, true);
        }

        private void DisplayControl_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            InputManager.OnMouseButtonChange(e.ChangedButton, false);
        }

        private void DisplayControl_OnKeyDown(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, true);
        }

        private void DisplayControl_OnKeyUp(object sender, KeyEventArgs e)
        {
            InputManager.OnKeyChange(e.Key, false);
        }

        private void DisplayControl_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            InputManager.OnMouseScroll(e.Delta);
        }
    }
}
