﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics.Core;
using InverseKinematics.OpenTK;
using OpenTK;
using PureCAD.Utils;
using Color = System.Windows.Media.Color;
using InputManager = InverseKinematics.Core.InputManager;

namespace InverseKinematics.Models
{
    public class SceneObstacle : Model, IColorable
    {
        public Color Color { get; set; } = Colors.Black;


        public Vector2[] Points;

        public float Width => Math.Abs(Points[0].X - Points[2].X);
        public float Height => Math.Abs(Points[0].Y - Points[2].Y);
        public Vector2 Anchor => _anchor;

        private VertexP[] _vertices;

        private Vector2 _anchor;

        private bool isPlaced = false;

        public SceneObstacle()
        {

            Shader = Shaders.BasicColorShader;
            Mesh = CreateRectangleMesh();
            Points = new Vector2[4];
            InputManager.RegisterOnButtonUpEvent(MouseButton.Right, () =>
            {
                isPlaced = true;
                _anchor = Points[0];
            });
        }

        public void StartDrawing()
        {
            _anchor = InputManager.MousePosition.ToOrthoWorldSpace(Simulation.Scene.Camera.OrthoX,
                Simulation.Scene.Camera.OrthoY, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
            isPlaced = false;
        }

        public Vector4 GetEdge(int idx)
        {
            return new Vector4(Points[idx].X, Points[idx].Y, Points[(idx + 1) % Points.Length].X, Points[(idx + 1) % Points.Length].Y);
        }

        private Mesh<VertexP> CreateRectangleMesh()
        {
            List<VertexP> vertices = new List<VertexP>();

            vertices.Add(new VertexP(-1, 1, 0));
            vertices.Add(new VertexP(1, 1, 0));
            vertices.Add(new VertexP(1, -1, 0));
            vertices.Add(new VertexP(-1, -1, 0));

            List<uint> edges = new List<uint>();

            edges.Add(0);
            edges.Add(3);
            edges.Add(1);

            edges.Add(1);
            edges.Add(3);
            edges.Add(2);

            _vertices = vertices.ToArray();

            return new Mesh<VertexP>(vertices, edges, MeshType.Triangles, AccessType.Dynamic);
        }

        public void UpdateRectangle(Vector2 anchor, float width, float height)
        {
            Points[0] = anchor;
            Points[1] = new Vector2(anchor.X + width, anchor.Y);
            Points[2] = new Vector2(anchor.X + width, anchor.Y + height);
            Points[3] = new Vector2(anchor.X, anchor.Y + height);

            for (int i = 0; i < Points.Length; i++)
            {
                _vertices[i] = new VertexP(new Vector3(Points[i].X, Points[i].Y, 0));
            }

            ((Mesh<VertexP>)Mesh).SetVertices(0, _vertices);
        }

        protected override void OnUpdate()
        {
            if (InputManager.IsMouseButtonDown(MouseButton.Right) && !isPlaced && Simulation.Settings.IsConfig)
            {
                var worldPos = InputManager.MousePosition.ToOrthoWorldSpace(Simulation.Scene.Camera.OrthoX,
                    Simulation.Scene.Camera.OrthoY, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
                UpdateRectangle(new Vector2(Math.Min(worldPos.X, _anchor.X), Math.Min(worldPos.Y, _anchor.Y)), Math.Abs(worldPos.X - _anchor.X), Math.Abs(worldPos.Y - _anchor.Y));
            }
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("model"), Transform.GetModelMatrix());
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            Shader.Bind(Shader.GetUniformLocation("color"), Color.ColorToVector3());
            Mesh.Draw();
        }
    }
}
