﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using InverseKinematics.Components;
using InverseKinematics.Core;
using InverseKinematics.OpenTK;
using InverseKinematics.Utils.Math;
using InverseKinematics.Utils.UIExtensionClasses;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PureCAD.Utils;
using InputManager = InverseKinematics.Core.InputManager;

namespace InverseKinematics.Models
{
    public class SceneArm : Model
    {
        public class Arm
        {
            private Mesh<VertexP> _mesh;
            public Color Color;
            private VertexP[] _vertices;

            public bool IsValid = true;

            public Arm(float l1, float l2, Color color)
            {
                _mesh = GenerateArmPartMesh(l1, l2);
                Color = color;
            }

            public void DrawMesh()
            {
                GL.LineWidth(2);
                _mesh.Draw();
                GL.LineWidth(1);
            }

            private Mesh<VertexP> GenerateArmPartMesh(float l1, float l2)
            {
                _vertices = new VertexP[3];
                _vertices[0] = new VertexP(Vector3.Zero);
                _vertices[1] = new VertexP(new Vector3(0, l1, 0));
                _vertices[2] = new VertexP(new Vector3(0, l1 + l2, 0));
                return new Mesh<VertexP>(_vertices, new List<uint>() { 0, 1, 1, 2 }, MeshType.Lines, AccessType.Dynamic);
            }

            public Vector4 GetArmPart(int idx)
            {
                return new Vector4(_vertices[idx].Position.X, _vertices[idx].Position.Y, _vertices[idx + 1].Position.X, _vertices[idx + 1].Position.Y);
            }

            public void SetPoint(int idx, Vector2 value)
            {
                _vertices[idx] = new VertexP(value.X, value.Y, 0);
                _mesh.SetVertex((uint)idx, ref _vertices[idx]);
            }

            public void Dispose()
            {
                _mesh.Dispose();
            }
        }

        public enum Solutions
        {
            One,
            Two,
            Both
        }

        public Arm ArmS1;

        public Arm ArmS2;

        public Arm ArmEndS1;
        public Arm ArmEndS2;

        public Solutions SelectedSolutionStart
        {
            get { return _selectedSolutionStart; }
            set
            {
                _selectedSolutionStart = value;
                RaisePropertyChanged();
            }
        }

        public Solutions SelectedSolutionEnd
        {
            get { return _selectedSolutionEnd; }
            set
            {
                _selectedSolutionEnd = value;
                RaisePropertyChanged();
            }
        }

        private float _l1;
        private float _l2;
        private Solutions _selectedSolutionStart = Solutions.One;
        private Solutions _selectedSolutionEnd = Solutions.One;
        private Vector2 _s1;
        private Vector2 _s2;
        private Vector2 _s1End;
        private Vector2 _s2End;

        public float L1
        {
            get { return _l1; }
            set
            {
                _l1 = value;
                ArmS1?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)))));
                ArmEndS1?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1End.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1End.X)))));
                //ArmS1?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S1.X + S1.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S1.X + S1.Y)))));
                ArmS2?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)))));
                ArmEndS2?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2End.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2End.X)))));
                //ArmS2?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S2.X + S2.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S2.X + S2.Y)))));
                RaisePropertyChanged();
            }
        }

        public float L2
        {
            get { return _l2; }
            set
            {
                _l2 = value;
                //ArmS1?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)))));
                ArmS1?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S1.X + S1.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S1.X + S1.Y)))));
                ArmEndS1?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1End.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S1End.X + S1End.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1End.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S1End.X + S1End.Y)))));
                //ArmS2?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)))));
                ArmS2?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S2.X + S2.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S2.X + S2.Y)))));
                ArmEndS2?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2End.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S2End.X + S2End.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2End.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S2End.X + S2End.Y)))));
                RaisePropertyChanged();
            }
        }

        public Vector2 S1
        {
            get { return _s1; }
            set
            {
                _s1 = value;
                ArmS1?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)))));
                ArmS1?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S1.X + S1.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S1.X + S1.Y)))));
                RaisePropertyChanged();
            }
        }

        public Vector2 S2
        {
            get { return _s2; }
            set
            {
                _s2 = value;
                ArmS2?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)))));
                ArmS2?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S2.X + S2.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S2.X + S2.Y)))));
                RaisePropertyChanged();
            }
        }

        public Vector2 S1End
        {
            get { return _s1End; }
            set
            {
                _s1End = value;
                ArmEndS1?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1End.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1End.X)))));
                ArmEndS1?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S1End.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S1End.X + S1End.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S1End.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S1End.X + S1End.Y)))));
                RaisePropertyChanged();
            }
        }

        public Vector2 S2End
        {
            get { return _s2End; }
            set
            {
                _s2End = value;
                ArmEndS2?.SetPoint(1, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2End.X))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2End.X)))));
                ArmEndS2?.SetPoint(2, new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(S2End.X)) + _l2 * Math.Cos(MathHelper.DegreesToRadians(S2End.X + S2End.Y))), (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(S2End.X)) + _l2 * Math.Sin(MathHelper.DegreesToRadians(S2End.X + S2End.Y)))));
                RaisePropertyChanged();
            }
        }

        //        public float A2
        //{
        //    get { return _a2; }
        //    set
        //    {
        //        _a2 = value;
        //        SecondArmS1?.SetMatrix(Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A2)) * Matrix4.CreateTranslation(0, L1, 0) * Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A1)));
        //        SecondArmS2?.SetMatrix(Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A2)) * Matrix4.CreateTranslation(0, L1, 0) * Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(A1)));
        //        RaisePropertyChanged();
        //    }
        //}

        public List<int> AnimationFrames;

        private bool _isAnimating;
        private float _animationTime = 2.5f;
        private float _animationFrame = 1 / 60.0f;
        private float _currentTime = 0.0f;
        private int _currentFrame = 0;

        public SceneArm()
        {
            AnimationFrames = new List<int>();
            Shader = Shaders.BasicColorShader;
            L1 = 100.0f;
            L2 = 100.0f;
            ArmS1 = new Arm(L1, L2, Colors.Red);
            ArmS2 = new Arm(L1, L2, Colors.Lime);
            ArmEndS1 = new Arm(L1, L2, Colors.Blue);
            ArmEndS2 = new Arm(L1, L2, Colors.Yellow);
            L1 = 100.0f;
            L2 = 100.0f;
        }

        public void SetupAnimationFrames(List<int> frames)
        {
            AnimationFrames = frames;
        }

        public void StartAnimation()
        {
            _isAnimating = true;

            _currentTime = 0;
            _currentFrame = 0;
            _animationFrame = (float)_animationTime / (float)AnimationFrames.Count;
        }

        public void EvaluateConfigurationSpace(MyBitmap bitmap)
        {
            for (int a1 = 0; a1 < 360; a1++)
            {
                for (int a2 = 0; a2 < 360; a2++)
                {
                    foreach (SceneObstacle obstacle in Simulation.Scene.SceneObjects.OfType<SceneObstacle>())
                    {
                        if (bitmap.GetPixel(a1, a2) == System.Drawing.Color.FromArgb(255, 0, 0, 0))
                            continue;

                        var p0 = Vector2.Zero;
                        var p1 = new Vector2((float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(a1))),
                            (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(a1))));
                        var p2 = new Vector2(
                            (float)(_l1 * Math.Cos(MathHelper.DegreesToRadians(a1)) +
                                     _l2 * Math.Cos(MathHelper.DegreesToRadians(a1 + a2))),
                            (float)(_l1 * Math.Sin(MathHelper.DegreesToRadians(a1)) +
                                     _l2 * Math.Sin(MathHelper.DegreesToRadians(a1 + a2))));

                        var arm1 = new Vector4(p0.X, p0.Y, p1.X, p1.Y);
                        var arm2 = new Vector4(p1.X, p1.Y, p2.X, p2.Y);
                        bitmap.SetPixel(a1, a2,
                            MathGeneralUtils.CheckArmLineObstacleIntersection(arm1, arm2, obstacle)
                                ? System.Drawing.Color.Black
                                : System.Drawing.Color.Gainsboro);
                    }
                }
            }
        }

        protected override void OnUpdate()
        {
            if (InputManager.IsMouseButtonDown(MouseButton.Left) && !_isAnimating)
            {
                Vector2 solution1, solution2;
                var worldCursorPos = InputManager.MousePosition.ToOrthoWorldSpace(Simulation.Scene.Camera.OrthoX,
                    Simulation.Scene.Camera.OrthoY, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
                if (MathGeneralUtils.CalculateInverseKinematics(worldCursorPos, L1, L2, out solution1, out solution2))
                {
                    S1 = solution1;
                    S2 = solution2;
                    //Console.WriteLine(S1);

                    //Console.WriteLine(S1.X < 0 ? S1.X + 360 : S1.X);
                    //Console.WriteLine(S1.Y < 0 ? S1.Y + 360 : S1.Y);
                    foreach (SceneObstacle obstacle in Simulation.Scene.SceneObjects.OfType<SceneObstacle>())
                    {
                        if (MathGeneralUtils.CheckArmObstacleIntersection(ArmS1, obstacle))
                        {
                            ArmS1.IsValid = false;
                            break;
                        }
                        else
                        {
                            ArmS1.IsValid = true;
                        }
                    }

                    foreach (SceneObstacle obstacle in Simulation.Scene.SceneObjects.OfType<SceneObstacle>())
                    {
                        if (MathGeneralUtils.CheckArmObstacleIntersection(ArmS2, obstacle))
                        {
                            ArmS2.IsValid = false;
                            break;
                        }
                        else
                        {
                            ArmS2.IsValid = true;
                        }
                    }
                }
            }
            if (InputManager.IsMouseButtonDown(MouseButton.Right) && !Simulation.Settings.IsConfig && !_isAnimating)
            {
                Vector2 solution1, solution2;
                var worldCursorPos = InputManager.MousePosition.ToOrthoWorldSpace(Simulation.Scene.Camera.OrthoX,
                    Simulation.Scene.Camera.OrthoY, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
                if (MathGeneralUtils.CalculateInverseKinematics(worldCursorPos, L1, L2, out solution1, out solution2))
                {
                    S1End = solution1;
                    S2End = solution2;

                    foreach (SceneObstacle obstacle in Simulation.Scene.SceneObjects.OfType<SceneObstacle>())
                    {
                        if (MathGeneralUtils.CheckArmObstacleIntersection(ArmEndS1, obstacle))
                        {
                            ArmEndS1.IsValid = false;
                            break;
                        }
                        else
                        {
                            ArmEndS1.IsValid = true;
                        }
                    }

                    foreach (SceneObstacle obstacle in Simulation.Scene.SceneObjects.OfType<SceneObstacle>())
                    {
                        if (MathGeneralUtils.CheckArmObstacleIntersection(ArmEndS2, obstacle))
                        {
                            ArmEndS2.IsValid = false;
                            break;
                        }
                        else
                        {
                            ArmEndS2.IsValid = true;
                        }
                    }
                }
            }

            if (_isAnimating)
            {
                _currentTime += Simulation.DeltaTime;
                if (_currentTime > _animationFrame)
                {
                    if (_currentFrame < AnimationFrames.Count)
                    {
                        var x = AnimationFrames[_currentFrame] % 360;
                        var y = AnimationFrames[_currentFrame] / 360;

                        //x = x >= 180 ? x - 360 : x;
                        //y = y >= 180 ? y - 360 : y;

                        S1 = new Vector2(x, y);
                        _currentFrame++;
                    }
                    else
                    {
                        _isAnimating = false;
                    }

                }
            }
        }

        protected override void OnRender()
        {
            Shader.Use();
            Shader.Bind(Shader.GetUniformLocation("view"), Simulation.Scene.Camera.GetViewMatrix());
            Shader.Bind(Shader.GetUniformLocation("projection"), Simulation.Scene.Camera.GetProjectionMatrix());
            if ((SelectedSolutionStart == Solutions.One || SelectedSolutionStart == Solutions.Both))
            {
                Shader.Bind(Shader.GetUniformLocation("color"), ArmS1.IsValid ? ArmS1.Color.ColorToVector3() : Colors.LightGray.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
                ArmS1.DrawMesh();
            }
            if ((SelectedSolutionStart == Solutions.Two || SelectedSolutionStart == Solutions.Both))
            {
                Shader.Bind(Shader.GetUniformLocation("color"), ArmS2.IsValid ? ArmS2.Color.ColorToVector3() : Colors.LightGray.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
                ArmS2.DrawMesh();
            }

            if (!Simulation.Settings.IsConfig && (SelectedSolutionEnd == Solutions.One || SelectedSolutionEnd == Solutions.Both))
            {
                Shader.Bind(Shader.GetUniformLocation("color"), ArmEndS1.IsValid ? ArmEndS1.Color.ColorToVector3() : Colors.LightGray.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
                ArmEndS1.DrawMesh();
            }
            if (!Simulation.Settings.IsConfig && (SelectedSolutionEnd == Solutions.Two || SelectedSolutionEnd == Solutions.Both))
            {
                Shader.Bind(Shader.GetUniformLocation("color"), ArmEndS2.IsValid ? ArmEndS2.Color.ColorToVector3() : Colors.LightGray.ColorToVector3());
                Shader.Bind(Shader.GetUniformLocation("model"), Matrix4.Identity);
                ArmEndS2.DrawMesh();
            }
        }

        public override void Dispose()
        {
            ArmS1.Dispose();
            ArmS2.Dispose();
            ArmEndS1.Dispose();
            ArmEndS2.Dispose();
            base.Dispose();
        }
    }
}
