﻿using System.Windows.Media;
using InverseKinematics.OpenTK;

namespace InverseKinematics.Core
{
    public interface IColorable
    {
        Color Color { get; set; }
    }

    public abstract class Model : SceneObject
    {
        public IMesh Mesh;
        public Shader Shader;
        
        protected Model()
        {
        }

        public override void Dispose()
        {
            base.Dispose();
            Mesh?.Dispose();
        }


    }
}
