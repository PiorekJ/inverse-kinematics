﻿using InverseKinematics.Utils.UIExtensionClasses;

namespace InverseKinematics.Core
{
    public abstract class Component : BindableObject
    {
        public SceneObject Owner;

        protected Component()
        {
        }

        public virtual void Dispose()
        {
        }
    }
}
