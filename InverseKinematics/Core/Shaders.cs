﻿using InverseKinematics.OpenTK;

namespace InverseKinematics.Core
{
    public static class Shaders
    {
        private static Shader _basicShader;
        public static Shader BasicShader
        {
            get
            {
                if (_basicShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/Basic/BasicShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/Basic/BasicShader.frag"))
                    {
                        _basicShader = new Shader(vert, frag);
                    }
                    return _basicShader;
                }
                return _basicShader;
            }
        }

        private static Shader _basicColorShader;
        public static Shader BasicColorShader
        {
            get
            {
                if (_basicColorShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicColor/BasicColorShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicColor/BasicColorShader.frag"))
                    {
                        _basicColorShader = new Shader(vert, frag);
                    }
                    return _basicColorShader;
                }
                return _basicColorShader;
            }
        }

        private static Shader _basicQuadShader;
        public static Shader BasicQuadShader
        {
            get
            {
                if (_basicQuadShader == null)
                {
                    using (ShaderObject vert = new VertexShaderObject("./Shaders/BasicQuad/BasicQuadShader.vert"),
                        frag = new FragmentShaderObject("./Shaders/BasicQuad/BasicQuadShader.frag"))
                    {
                        _basicQuadShader = new Shader(vert, frag);
                    }
                    return _basicQuadShader;
                }
                return _basicQuadShader;
            }
        }
    }
}
