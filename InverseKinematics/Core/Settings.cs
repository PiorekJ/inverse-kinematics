﻿using InverseKinematics.Utils.UIExtensionClasses;

namespace InverseKinematics.Core
{
    public class Settings : BindableObject
    {
        private bool _isConfig = true;

        public bool IsConfig
        {
            get { return _isConfig; }
            set
            {
                _isConfig = value; 
                RaisePropertyChanged();
            }
        }
    }
}
