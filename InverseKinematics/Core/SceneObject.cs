﻿using System;
using System.Collections.Generic;
using InverseKinematics.Components;
using InverseKinematics.Utils.UIExtensionClasses;

namespace InverseKinematics.Core
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }



    public class SceneObject : BindableObject, IDisposable
    {
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = value;
                RaisePropertyChanged();
            }
        }

        public Transform Transform { get; set; }

        private List<Component> _components;

        public delegate void DeleteHandler();

        public event DeleteHandler OnDeleteEvent;

        #region UI Related

        public bool IsVisibleOnList
        {
            get { return _isVisibleOnList; }
            set
            {
                _isVisibleOnList = value;
                RaisePropertyChanged();
            }
        }
        private bool _isVisibleOnList = true;

        public string ObjectName
        {
            get => _objectName;
            set
            {
                _objectName = value;
                RaisePropertyChanged();
            }
        }

        private string _objectName;
        private bool _isVisible = true;

        #endregion

        public SceneObject()
        {
            _components = new List<Component>();
            Transform = AddComponent<Transform>();
        }

        public TComponent AddComponent<TComponent>()
            where TComponent : Component, new()
        {
            TComponent component = new TComponent { Owner = this };
            _components.Add(component);
            return component;
        }

        public TComponent GetComponent<TComponent>()
            where TComponent : Component
        {
            foreach (var item in _components)
            {
                if (item is TComponent)
                {
                    return (TComponent)item;
                }
            }
            return null;
        }

        public void RemoveComponent(Component component)
        {
            component.Dispose();
            _components.Remove(component);
        }


        protected virtual void OnRender()
        {

        }

        protected virtual void OnUpdate()
        {

        }

        public void Render()
        {
            if (!IsVisible)
                return;

            OnRender();
        }

        public virtual void Update()
        {
            OnUpdate();
        }

        public void OnDelete()
        {
            OnDeleteEvent?.Invoke();
        }

        public virtual void Dispose()
        {
            for (var i = _components.Count - 1; i >= 0; i--)
            {
                Component component = _components[i];
                RemoveComponent(component);
            }
        }

    }
}
