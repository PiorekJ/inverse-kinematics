﻿using InverseKinematics.Utils;

namespace InverseKinematics.Core
{
    public class Simulation
    {
        public static Scene Scene { get; set; }
        public static Settings Settings { get; set; }
        public static Storage Storage { get; set; }
        public TimeCounter TimeCounter;

        public static float DeltaTime => TimeCounter.DeltaTime;

        public Simulation()
        {
            Scene = new Scene();
            Settings = new Settings();
            Storage = new Storage();
            TimeCounter = new TimeCounter();
        }

        public void InitializeSimulation()
        {
            TimeCounter.Start();
            Scene.AddSceneBackground();
            Scene.AddSceneCursor();
            Scene.AddSceneArm();
        }
    }
}
