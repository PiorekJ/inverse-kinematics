﻿using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using InverseKinematics.Models;
using InverseKinematics.Utils.Math;
using InverseKinematics.Utils.UIExtensionClasses;
using PureCAD.Utils;
using Color = System.Drawing.Color;

namespace InverseKinematics.Core
{
    public class Scene : BindableObject
    {
        private SceneArm _sceneArm;
        private WriteableBitmap _configurationDisplayBitmap;
        private MyBitmap _configurationBitmap;
        public Camera Camera { get; set; }
        public SceneCursor SceneCursor { get; set; }
        public ObservableCollection<SceneObject> SceneObjects { get; set; }
        public SceneBackground SceneBackground { get; set; }

        public MyBitmap ConfigurationBitmap
        {
            get { return _configurationBitmap; }
            set
            {
                _configurationBitmap = value;
                
                RaisePropertyChanged();
            }
        }

        public WriteableBitmap ConfigurationDisplayBitmap
        {
            get { return _configurationDisplayBitmap; }
            set
            {
                _configurationDisplayBitmap = value; 
                RaisePropertyChanged();
            }
        }

        public SceneArm SceneArm
        {
            get { return _sceneArm; }
            set
            {
                _sceneArm = value;
                RaisePropertyChanged();
            }
        }

        #region Commands

        public ICommand EvaluateConfigurationSpaceCommand { get; set; }

        public ICommand ConfigSwitchCommand { get; set; }

        public ICommand FindPathCommand { get; set; }

        public ICommand StartAnimationCommand { get; set; }

        #endregion

        public Scene()
        {
            Camera = new Camera();
            SceneObjects = new ObservableCollection<SceneObject>();

            EvaluateConfigurationSpaceCommand = new RelayCommand(param =>
            {
                ConfigurationBitmap = new MyBitmap(360, 360);
                SceneArm.EvaluateConfigurationSpace(ConfigurationBitmap);
                ConfigurationDisplayBitmap = _configurationBitmap.WriteableBitmap;
            });

            ConfigSwitchCommand = new RelayCommand(param =>
            {
                Simulation.Settings.IsConfig = !Simulation.Settings.IsConfig;
                if (!Simulation.Settings.IsConfig)
                {
                    EvaluateConfigurationSpaceCommand.Execute(null);
                }
            });

            StartAnimationCommand = new RelayCommand(param =>
            {
                if (SceneArm.AnimationFrames != null && SceneArm.AnimationFrames.Count > 0)
                {
                    SceneArm.StartAnimation();
                }
            });

            FindPathCommand = new RelayCommand(param =>
            {
                if (!Simulation.Settings.IsConfig)
                {
                    SceneArm.AnimationFrames.Clear();
                    ConfigurationBitmap = new MyBitmap(360, 360);
                    SceneArm.EvaluateConfigurationSpace(ConfigurationBitmap);
                    ConfigurationDisplayBitmap = _configurationBitmap.WriteableBitmap;
                }
                var pixels = ConfigurationBitmap.GetPixels();
                var normalizedSX = SceneArm.S1.X < 0 ? (int)SceneArm.S1.X + 360 : (int)SceneArm.S1.X;
                var normalizedSY = SceneArm.S1.Y < 0 ? (int)SceneArm.S1.Y + 360 : (int)SceneArm.S1.Y;
                var normalizedEX = SceneArm.S1End.X < 0 ? (int)SceneArm.S1End.X + 360 : (int)SceneArm.S1End.X;
                var normalizedEY = SceneArm.S1End.Y < 0 ? (int)SceneArm.S1End.Y + 360 : (int)SceneArm.S1End.Y;

                var path = MathGeneralUtils.Traverse(ref pixels, 360, (int)(normalizedSX + normalizedSY * 360),
                    (int)(normalizedEX + normalizedEY * 360));

                if (path != null)
                {
                    ConfigurationBitmap = new MyBitmap(360, 360);
                    for (int i = 0; i < 360; i++)
                    {
                        for (int j = 0; j < 360; j++)
                        {
                            ConfigurationBitmap.SetPixel(i, j, pixels[i + 360 * j]);
                        }
                    }
                    ConfigurationDisplayBitmap = _configurationBitmap.WriteableBitmap;

                    SceneArm.SetupAnimationFrames(path);
                }
                
            });

            InputManager.RegisterOnButtonDownEvent(MouseButton.Right, () =>
            {
                var item = AddSceneObstacle();
                item.StartDrawing();
            });

            InputManager.RegisterOnKeyDownEvent(Key.Delete, () =>
            {
                var sceneObstacles = SceneObjects.OfType<SceneObstacle>().ToArray();
                for (int i = SceneObjects.OfType<SceneObstacle>().ToArray().Length - 1; i >= 0; i--)
                {
                    var obstacle = sceneObstacles[i];
                    var worldPos = InputManager.MousePosition.ToOrthoWorldSpace(Simulation.Scene.Camera.OrthoX,
                        Simulation.Scene.Camera.OrthoY, Simulation.Storage.DisplayWidth, Simulation.Storage.DisplayHeight);
                    if (MathGeneralUtils.IsPointInsideRectangle(obstacle.Anchor, obstacle.Width, obstacle.Height,
                        worldPos))
                    {
                        RemoveSceneObject(obstacle);
                    }
                }

            });
        }

        public SceneObstacle AddSceneObstacle()
        {
            var item = new SceneObstacle();
            SceneObjects.Add(item);
            return item;
        }

        public SceneCursor AddSceneCursor()
        {
            SceneCursor = new SceneCursor();
            return SceneCursor;
        }

        public SceneBackground AddSceneBackground()
        {
            SceneBackground = new SceneBackground();
            return SceneBackground;
        }

        public SceneArm AddSceneArm()
        {
            SceneArm = new SceneArm();
            return SceneArm;
        }


        public void RemoveSceneObject(SceneObject item)
        {
            if (item != null)
            {
                item.OnDelete();
                SceneObjects.Remove(item);
                item.Dispose();
            }
        }
    }
}