using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace InverseKinematics.Utils.Converters
{
    class BooleanVisibilityConverter : IValueConverter
    {
        public Visibility True { get; set; }
        public Visibility False { get; set; }

        public BooleanVisibilityConverter()
        {
        }

        public BooleanVisibilityConverter(Visibility trueValue, Visibility falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is bool && ((bool)value) ? True : False;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is Visibility && EqualityComparer<Visibility>.Default.Equals((Visibility)value, True);
        }
    }
}