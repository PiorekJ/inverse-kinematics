﻿using System.Windows.Media;
using OpenTK;

namespace InverseKinematics.Utils
{
    public static class Constants
    {
        public const int LineSize = 2;

        public static Vector3 DefaultCameraPosition = new Vector3(0, 0, 0);
        public const float DefaultFOV = (float)System.Math.PI / 4;
        public const float DefaultZNear = 1f;
        public const float DefaultZFar = -1f;

        public const float CameraMovementMouseSensitivity = 30.0f;
        public const float CameraRotationMouseSensitivity = 0.05f;
        public const float CameraZoomMouseSensitivity = 1.0f;

        public const float CameraMovementKeyVelocity = 300.0f;
        public const float CameraMovementKeySlowVelocity = 150.0f;

        public static readonly Color BackgroundColor = Colors.White;
        public static readonly Color StripColor = Colors.WhiteSmoke;

        public const float Epsilon = 0.000001f;
    }
}
