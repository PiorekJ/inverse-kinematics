﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Media;
using InverseKinematics.Models;
using OpenTK;
using Color = System.Drawing.Color;

namespace InverseKinematics.Utils.Math
{
    public static class MathGeneralUtils
    {
        public static Matrix4 CreatePerspectiveFOVMatrix(float fov, float aspectRatio, float zNear, float zFar)
        {

            if (fov <= 0 || fov > System.Math.PI)
                throw new ArgumentOutOfRangeException("Incorrect fov");
            if (aspectRatio <= 0)
                throw new ArgumentOutOfRangeException("Incorrect aspect ratio");
            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("Incorrect zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("Incorrect zNear");

            float yMax = zNear * (float)System.Math.Tan(0.5f * fov);
            float yMin = -yMax;
            float xMin = yMin * aspectRatio;
            float xMax = yMax * aspectRatio;

            if (zNear <= 0)
                throw new ArgumentOutOfRangeException("zNear");
            if (zFar <= 0)
                throw new ArgumentOutOfRangeException("zFar");
            if (zNear >= zFar)
                throw new ArgumentOutOfRangeException("zNear");

            float x = (2.0f * zNear) / (xMax - xMin);
            float y = (2.0f * zNear) / (yMax - yMin);
            float a = (xMax + xMin) / (xMax - xMin);
            float b = (yMax + yMin) / (yMax - yMin);
            float c = -(zFar + zNear) / (zFar - zNear);
            float d = -(2.0f * zFar * zNear) / (zFar - zNear);

            return new Matrix4(x, 0, 0, 0,
                0, y, 0, 0,
                a, b, c, -1,
                0, 0, d, 0);
        }

        public static Matrix4 CreateTranslation(Vector3 position)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row3 = new Vector4(position.X, position.Y, position.Z, 1.0f);
            return result;
        }

        public static Matrix4 CreateFromQuaternion(Quaternion rotation)
        {
            rotation.ToAxisAngle(out var axis, out var angle);

            float cos = (float)System.Math.Cos(-angle);
            float sin = (float)System.Math.Sin(-angle);
            float t = 1.0f - cos;

            axis.Normalize();

            Matrix4 result = Matrix4.Identity;
            result.Row0 = new Vector4(t * axis.X * axis.X + cos, t * axis.X * axis.Y - sin * axis.Z, t * axis.X * axis.Z + sin * axis.Y, 0.0f);
            result.Row1 = new Vector4(t * axis.X * axis.Y + sin * axis.Z, t * axis.Y * axis.Y + cos, t * axis.Y * axis.Z - sin * axis.X, 0.0f);
            result.Row2 = new Vector4(t * axis.X * axis.Z - sin * axis.Y, t * axis.Y * axis.Z + sin * axis.X, t * axis.Z * axis.Z + cos, 0.0f);
            result.Row3 = Vector4.UnitW;
            return result;
        }

        public static Matrix4 CreateScale(Vector3 scale)
        {
            Matrix4 result = Matrix4.Identity;
            result.Row0.X = scale.X;
            result.Row1.Y = scale.Y;
            result.Row2.Z = scale.Z;
            return result;
        }

        public static bool CalculateInverseKinematics(Vector2 pointToReach, float l1, float l2, out Vector2 solution1, out Vector2 solution2)
        {
            solution1 = new Vector2();
            solution2 = new Vector2();

            float l3 = pointToReach.Length;
            var atan2 = MathHelper.RadiansToDegrees((float)System.Math.Atan2(pointToReach.Y, pointToReach.X));

            if (l1 + l2 < l3)
            {
                solution1 = new Vector2(atan2, 0.0f);
                solution2 = new Vector2(atan2, 0.0f);
                return true;
            }

            if (l1 + l2 <= l3 || l1 + l3 <= l2 || l2 + l3 <= l1)
            {
                if (System.Math.Abs(l1 - l2) < Constants.Epsilon && pointToReach == Vector2.Zero)
                {
                    solution1 = new Vector2(0, 180.0f);
                    solution2 = new Vector2(0, 180.0f);
                    return true;
                }
                return false;
            }

            var cosA1 = ((l3 * l3) + (l1 * l1) - (l2 * l2)) / (2 * l3 * l1);
            var a1 = MathHelper.RadiansToDegrees((float)System.Math.Acos(cosA1));

            var cosA2 = ((l2 * l2) + (l1 * l1) - (l3 * l3)) / (2 * l2 * l1);
            var b2 = MathHelper.RadiansToDegrees((float)System.Math.Acos(cosA2));

            solution1 = new Vector2(atan2 - a1, 180.0f - b2);
            solution2 = new Vector2(atan2 + a1, 180.0f + b2);
            return true;

        }

        public static bool AreIntersecting(Vector2 from1, Vector2 to1, Vector2 from2, Vector2 to2)
        {
            float d1 = Determinant(to2 - from2, from1 - from2);
            float d2 = Determinant(to2 - from2, to1 - from2);
            float d3 = Determinant(to1 - from1, from2 - from1);
            float d4 = Determinant(to1 - from1, to2 - from1);

            float d12 = d1 * d2;
            float d34 = d3 * d4;

            if (d12 > 0 || d34 > 0) return false;

            if (d12 < 0 || d34 < 0)
            {
                return true;
            }

            return false;
        }

        private static float Determinant(Vector2 v1, Vector2 v2)
        {
            return v1.X * v2.Y - v1.Y * v2.X;
        }

        public static bool CheckArmObstacleIntersection(SceneArm.Arm arm, SceneObstacle obstacle)
        {
            for (int i = 0; i < 2; i++)
            {
                var armPoints = arm.GetArmPart(i);
                for (int j = 0; j < 4; j++)
                {
                    var obstacleEdge = obstacle.GetEdge(j);
                    if (AreIntersecting(armPoints.Xy, armPoints.Zw, obstacleEdge.Xy, obstacleEdge.Zw))
                        return true;
                }
            }
            return false;
        }

        public static bool CheckArmLineObstacleIntersection(Vector4 line1, Vector4 line2, SceneObstacle obstacle)
        {
            for (int j = 0; j < 4; j++)
            {
                var obstacleEdge = obstacle.GetEdge(j);
                if (AreIntersecting(line1.Xy, line1.Zw, obstacleEdge.Xy, obstacleEdge.Zw) || AreIntersecting(line2.Xy, line2.Zw, obstacleEdge.Xy, obstacleEdge.Zw))
                    return true;
                if (IsPointInsideRectangle(obstacle.Anchor, obstacle.Width, obstacle.Height, line1.Xy) ||
                    IsPointInsideRectangle(obstacle.Anchor, obstacle.Width, obstacle.Height, line1.Zw) ||
                    IsPointInsideRectangle(obstacle.Anchor, obstacle.Width, obstacle.Height, line2.Xy) ||
                    IsPointInsideRectangle(obstacle.Anchor, obstacle.Width, obstacle.Height, line2.Zw))
                    return true;
            }

            return false;
        }

        public static bool IsPointInsideRectangle(Vector2 rectAnchor, float rectWidth, float rectHeight, Vector2 pointPosition)
        {
            return pointPosition.X > rectAnchor.X && pointPosition.X < rectAnchor.X + rectWidth &&
                   pointPosition.Y > rectAnchor.Y && pointPosition.Y < rectAnchor.Y + rectHeight;
        }

        public static List<int> Traverse(ref Color[] bitmapPixels, int size, int startIdx, int endIdx)
        {
            var q = new Queue();
            q.Enqueue(startIdx);

            var visited = new bool[bitmapPixels.Length];
            for (int j = 0; j < 360; j++)
            {
                for (int k = 0; k < 360; k++)
                {
                    if (bitmapPixels[j + 360 * k] == Color.FromArgb(255, 0, 0, 0))
                    {
                        visited[j + 360 * k] = true;
                    }
                }

            }

            if (visited[startIdx])
            {
                MessageBox.Show("Start is incorrect!");
                return null;
            }

            visited[startIdx] = true;

            var prev = new int[bitmapPixels.Length];
            for (int i = 0; i < prev.Length; i++)
            {
                prev[i] = -1;
            }

            while (q.Count != 0)
            {
                var p = (int)q.Dequeue();
                var neighbours = GetNeighbours(p, size);
                foreach (int neighbour in neighbours)
                {
                    if (!visited[neighbour])
                    {
                        q.Enqueue(neighbour);
                        visited[neighbour] = true;
                        prev[neighbour] = p;

                        if (neighbour == endIdx)
                            return GetPath(ref bitmapPixels, startIdx, endIdx, prev);
                    }
                }
                
            }

            return GetPath(ref bitmapPixels, startIdx, endIdx, prev);
        }

        private static List<int> GetPath(ref Color[] bitmapPixels, int startIdx, int endIdx, int[] prev)
        {
            var path = new List<int>();

            for (int i = endIdx; i != -1; i = prev[i])
            {
                path.Add(i);
                bitmapPixels[i] = Color.Red;
            }

            path.Reverse();
            Console.WriteLine("Path found: " + (path[0] == startIdx).ToString());
            return path[0] == startIdx ? path : null;
        }

        private static IEnumerable<int> GetNeighbours(int source, int size)
        {
            int x = source % size;
            int y = source / size;

            var neighbourList = new List<int>();

            if (y - 1 < 0)
                neighbourList.Add(x + (size - 1) * size);
            else
                neighbourList.Add(x + (y - 1) * size);

            if (y + 1 >= size)
                neighbourList.Add(x);
            else
                neighbourList.Add(x + (y + 1) * size);

            if (x - 1 < 0)
                neighbourList.Add((size - 1) + y * size);
            else
                neighbourList.Add((x - 1) + y * size);

            if (x + 1 >= size)
                neighbourList.Add(y * size);
            else
                neighbourList.Add((x + 1) + y * size);

            //if (y - 1 < 0)
            //    neighbourList.Add(x + y * size);
            //else
            //    neighbourList.Add(x + (y - 1) * size);

            //if (y + 1 >= size)
            //    neighbourList.Add(x + (size - 1) * size);
            //else
            //    neighbourList.Add(x + (y + 1) * size);

            //if (x - 1 < 0)
            //    neighbourList.Add(x + y * size);
            //else
            //    neighbourList.Add((x - 1) + y * size);

            //if (x + 1 >= size)
            //    neighbourList.Add(y * size);
            //else
            //    neighbourList.Add((size - 1) + y * size);

            return neighbourList;
        }
    }
}
