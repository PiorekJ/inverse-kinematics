﻿using System;
using System.Windows.Media;
using OpenTK;

namespace PureCAD.Utils
{
    public static class Extensions
    {
        public static Vector3 ColorToVector3(this Color color)
        {
            return new Vector3(color.ScR, color.ScG, color.ScB);
        }

        public static Vector3 EulerAngles(this Quaternion q)
        {
            double heading, attitude, bank;
            double test = q.X * q.Y + q.Z * q.W;
            Vector3 euler = Vector3.Zero;

            if (test > 0.499)
            { // singularity at north pole
                heading = 2 * System.Math.Atan2(q.X, q.W);
                attitude = System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            if (test < -0.499)
            { // singularity at south pole
                heading = -2 * System.Math.Atan2(q.X, q.W);
                attitude = -System.Math.PI / 2;
                bank = 0;
                euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
                return euler;
            }
            double sqx = q.X * q.X;
            double sqy = q.Y * q.Y;
            double sqz = q.Z * q.Z;
            heading = System.Math.Atan2(2 * q.Y * q.W - 2 * q.X * q.Z, 1 - 2 * sqy - 2 * sqz);
            attitude = System.Math.Asin(2 * test);
            bank = System.Math.Atan2(2 * q.X * q.W - 2 * q.Y * q.Z, 1 - 2 * sqx - 2 * sqz);
            euler = new Vector3((float)MathHelper.RadiansToDegrees(bank), (float)MathHelper.RadiansToDegrees(heading), (float)MathHelper.RadiansToDegrees(attitude));
            return euler;
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static Vector2 ToOrthoWorldSpace(this Vector2 vector, float orthoX, float orthoY, int displayWidth,
            int displayHeight)
        {
            return new Vector2((vector.X / displayWidth * 2 * orthoX - orthoX), -(vector.Y / displayHeight * 2 * orthoY - orthoY));
        }

        

        //public static Vector3 GetIntersection(Vector3 f1, Vector3 t1, Vector3 f2, Vector3 t2)
        //{
        //    float d1 = Det(t2 - f2, f1 - f2);
        //    float d2 = Det(t2 - f2, t1 - f2);
        //    float d3 = Det(t1 - f1, f2 - f1);
        //    float d4 = Det(t1 - f1, t2 - f1);

        //    float d12 = d1 * d2;
        //    float d34 = d3 * d4;

        //    if (d12 > 0 || d34 > 0) return null;

        //    if (d12 < 0 || d34 < 0)
        //    {
        //        Vector3 s = t2 - f2;
        //        Vector3 r = t1 - f1;

        //        float t = Det(f2 - f1, s) / Det(r, s);

        //        return f1 + t * r;
        //    }

        //    return null;
        //}

        //private static float Det(Vector3 v1, Vector3 v2)
        //{
        //    return v1.X * v2.Y - v1.Y * v2.X;
        //}
    }
}
