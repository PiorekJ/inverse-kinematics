﻿using System.Collections.Generic;
using InverseKinematics.Core;
using InverseKinematics.Utils.UIExtensionClasses;

namespace InverseKinematics.Utils
{
    public class Storage : BindableObject
    {
        public int DisplayWidth;
        public int DisplayHeight;
        public float DisplayAspect;
        
        public void SetDisplayDimensions(int displayWidth, int displayHeight, float displayAspect)
        {
            DisplayWidth = displayWidth;
            DisplayHeight = displayHeight;
            DisplayAspect = displayAspect;
        }
    }
}
